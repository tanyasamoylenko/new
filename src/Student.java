import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Администратор on 19.11.14.
 */
public class Student extends People implements Serializable{
    private int id;
    private int group_id;
    private ArrayList<Mark> marks;


    public Student(int id, String name, String tel, String address, int group_id, ArrayList<Mark> marks) {
        super(name, tel, address);
        this.id = id;
        this.group_id = group_id;
        this.marks = marks;
    }

    public void setId(int id){
        this.id = id;
    }



    public int getId(){
        return id;
    }

    public int getGroup_id(){
        return group_id;
    }

    public void addMark(Mark mark){
        marks.add(mark);
    }

    public ArrayList<Mark> getMarks(){
        return marks;
    }

    public void printMarks(){
        for(Mark m: marks)
            System.out.print(m.getMark()+" ");
        System.out.println();
    }

    public int averageMark(){
        int sum = 0;
        for(Mark m: marks){
            sum+=m.getMark();
        }
        return sum/marks.size();
    }




    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("name: ");
        sb.append(getName());
        sb.append("; tel: ");
        sb.append(getTel());
        sb.append("; address: ");
        sb.append(getAddress());
        return sb.toString();
    }

    @Override
    public boolean equals(Object o){
        if(this==o)
            return true;
        if(o==null || o.getClass()!=getClass())
            return false;
        if(o instanceof Student){
            Student student = (Student)o;
            if(id!=student.id)
                return false;
            if(!getName().equals(student.getName()))
                return false;
            if(!getTel().equals(student.getTel()))
                return false;
            if(!getAddress().equals(student.getAddress()))
                return false;
            if(group_id!=student.group_id)
                return false;
            if(!marks.equals(student.marks))
                return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result += prime*result+id;
        result += prime*result+getName().hashCode();
        result += prime*result+getTel().hashCode();
        result += prime*result+getAddress().hashCode();
        result += prime*result+group_id;
        result += prime*result+marks.hashCode();
        return result;
    }
}
