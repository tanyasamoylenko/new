/**
 * Created by Администратор on 19.11.14.
 */
public abstract class People {
    private String name;
    private String tel;
    private String address;

    People(){
    }

    People(String name, String tel, String address){
        this.name = name;
        this.tel = tel;
        this.address = address;
    }

    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    public String getTel(){
        return tel;
    }

    public String getAddress(){
        return address;
    }

}
