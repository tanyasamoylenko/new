import java.io.*;

/**
 * Created by Администратор on 25.11.14.
 */
public class ReaderWriter {
    private static final  String FILE_PATH = "C:\\Users\\Администратор\\IdeaProjects\\IODemo\\files\\file.txt";
    private static final  String TEST_LINE = "Test line";
    public static void main(String[] args){
        try {
            FileWriter fw = new FileWriter(FILE_PATH);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(TEST_LINE);
            bw.close();
            fw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            FileReader fr = new FileReader(FILE_PATH);
            LineNumberReader lr = new LineNumberReader(fr);
            int i = lr.getLineNumber();
            String str = lr.readLine();
            System.out.println(i);
            System.out.println(str);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
