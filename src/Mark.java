import java.io.Serializable;

/**
 * Created by Администратор on 19.11.14.
 */
public class Mark implements Serializable{
    private int id;
    private int mark;
    private int studentId;

    public Mark(){
    }

    public Mark(int id, int mark, int studentId){
        this.id = id;
        this.mark = mark;
        this.studentId = studentId;
    }

    public int getId(){
        return id;
    }

    public int getMark(){
        return mark;
    }

    public int getStudentId(){
        return studentId;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("id: ");
        sb.append(id);
        sb.append("; mark: ");
        sb.append(mark);
        sb.append("; student_id: ");
        sb.append(studentId);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o){
        if(this==o)
            return true;
        if(o==null || o.getClass()!=getClass())
            return false;
        if(o instanceof Mark){
            Mark m = (Mark)o;
            if(id!=m.id)
                return false;
            if(mark!=m.mark)
                return false;
            if(studentId!=m.studentId)
                return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result += prime*result+id;
        result += prime*result+mark;
        result += prime*result+studentId;
        return result;
    }
}
