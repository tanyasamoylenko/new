import java.io.*;

/**
 * Created by Администратор on 26.11.14.
 */
public class SerializDemo {
    public static void saveStudent(Student student){

        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\Users\\Администратор\\IdeaProjects\\GradeBook\\serialize"));
            oos.writeObject(student);
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Student loadStudent(){
        Student student = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\Users\\Администратор\\IdeaProjects\\GradeBook\\serialize"));
            student = (Student)ois.readObject();
            ois.close();
        }catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return student;
    }
    public static void main(String[] args){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\Users\\Администратор\\IdeaProjects\\IODemo\\files\\serializ"));
            MyClass m1 = new MyClass("One", 1.1, 3);
            oos.writeObject(m1);
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\Users\\Администратор\\IdeaProjects\\IODemo\\files\\serializ"));
            MyClass m2 = (MyClass) ois.readObject();
            System.err.println(m2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

class MyClass implements Serializable{
    static String str;
    double d;
    transient int i;

    public MyClass(String str, double d, int i){
        this.str=str;
        this.d=d;
        this.i=i;
    }

    @Override
    public String toString(){
        return "str: "+str+";  d: "+d+";  i: "+i;
    }
}
