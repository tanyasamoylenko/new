import java.util.Date;
import java.util.Formatter;

/**
 * Created by Администратор on 26.11.14.
 */
public class FormatterDemo {

    public static String formatDouble(double d){
        StringBuilder sb = new StringBuilder();
        Formatter f = new Formatter(sb);
        f.format("%.2f", d);
        return sb.toString();
    }

    public static String formatDate(Date date){
        StringBuilder sb = new StringBuilder();
        Formatter f = new Formatter(sb);
        f.format("%td.%tm.%ty", date, date, date);
        return sb.toString();
    }
    public static void main(String[] args){
        StringBuilder sb = new StringBuilder();
        Formatter f = new Formatter(sb);
        boolean b = true;
        int i = 3;
        double d = 2.2454;
        String str = "Ok";
        f.format("This is the %b, that %d, and %5.2f, %s", b, i, d, str);
        System.out.println(sb);
        System.out.println(formatDouble(2.56899));
        System.out.format("%08d %n", 23);
        System.out.format("%+8d %n", 23);
        System.out.format("%08d %n", 23);

        Date date = new Date();
        System.out.println(formatDate(date));
    }

}
