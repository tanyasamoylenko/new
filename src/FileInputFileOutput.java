import java.io.*;

/**
 * Created by Администратор on 25.11.14.
 */
public class FileInputFileOutput {
    private static final  String FILE_PATH = "C:\\Users\\Администратор\\IdeaProjects\\IODemo\\files\\file2.txt";

    public static void main(String[] args){
        File file = null;
        FileInputStream fis = null;
        FileOutputStream fot = null;
        try {
            file = new File(FILE_PATH);
            fot = new FileOutputStream(file);
            String str = "This is the test";
            byte[] bytes = str.getBytes();
            fot.write(bytes);
            fot.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            file = new File(FILE_PATH);
            fis = new FileInputStream(file);
            String str="";
            byte[] bytes = new byte[fis.available()];
            fis.read(bytes);
            for(int i=0; i<bytes.length; i++){
                str+=(char)bytes[i];
            }
            System.out.println(str);
            fis.close();

        }catch (IOException e){
            e.printStackTrace();
        }


    }
}
