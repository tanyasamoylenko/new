import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by Администратор on 24.11.14.
 */
public class IODemo {

    private static final String FILE_PATH = "C:\\Users\\Администратор\\IdeaProjects\\IODemo\\files\\file.txt";

    public static void createDirAndFile(){
        try {
            File f = new File("C:\\Users\\Администратор\\IdeaProjects\\IODemo\\test");
            f.mkdir();
            f = new File("C:\\Users\\Администратор\\IdeaProjects\\IODemo\\test\\test.txt");
            f.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void deleteDirAndFile(){
        File f = new File("C:\\Users\\Администратор\\IdeaProjects\\IODemo\\test\\test.txt");
        f.delete();
        f = new File("C:\\Users\\Администратор\\IdeaProjects\\IODemo\\test");
        f.delete();
    }

    public static void randomAccessWrite(){
        try {
            RandomAccessFile r = new RandomAccessFile(FILE_PATH, "rw");
            r.writeInt(10);
            char[] str = ("test line").toCharArray();
            for(char c: str)
                r.writeChar(c);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void randomAccessRead(){
        try {
            RandomAccessFile r = new RandomAccessFile(FILE_PATH, "rw");
            r.seek(15);
            System.out.println(r.readLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void randomAccessWriteUTF() {
        try{
            RandomAccessFile r = new RandomAccessFile(FILE_PATH, "rw");
            r.writeUTF("test line");
            r.writeUTF("test line 2");
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static String randomAccessReadUTF() {
        try{
            RandomAccessFile r = new RandomAccessFile(FILE_PATH, "rw");
            r.skipBytes(11);
            String str = r.readUTF();
            System.out.println(str);
            return str;
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        } 		return null;
    }

    public static void main(String[] args){
        File file = new File("C:\\Users\\Администратор\\IdeaProjects\\IODemo\\files");
        System.out.println("name: "+file.getName());
        System.out.println("parent: "+file.getParent());
        System.out.println("path: "+file.getPath());
        System.out.println("absolut path: "+file.getAbsolutePath());
        System.out.println("exists: "+file.exists());
        System.out.println("is file: "+file.isFile());
        System.out.println("can read: "+file.canRead());
        System.out.println("can write: "+file.canWrite());
        System.out.println("is directory: "+file.isDirectory());
        String[] names = file.list();
        for(String n: names)
            System.out.println(n);
        randomAccessWriteUTF();
        randomAccessReadUTF();
    }

}
